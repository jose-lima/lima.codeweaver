﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Lima.CodeWeaver.Compiler {

    public class Weaver {

        private static readonly string weaverPath = 
            @"C:\Users\user1\source\repos\CodeWeaver\Lima.CodeWeaver.Compiler\Weaver.cs";

        private static void TimeMethod() {

            var stopWatch = Stopwatch.StartNew();

            //$OriginalMethodBody

            stopWatch.Stop();
            var elapsedTimeMs = stopWatch.ElapsedMilliseconds;
            Console.WriteLine($"({elapsedTimeMs}ms)");
        }

        internal static string TimeClassMethod(string classFullPath, string methodToOrnament, string outputPath = null) {
          
            var timingTemplateMethod = GetMethod(weaverPath, nameof(TimeMethod), out _);
            var timingStatements = timingTemplateMethod.Body.Statements.ToList();
            var msg = ConsoleWrite($"Method '{methodToOrnament}' executed ");
            var ornamentedMethodClass = OrnamentClassMethod(classFullPath, methodToOrnament, 
                timingStatements.Take(1), new[] { msg }.Concat(timingStatements.Skip(1)), outputPath);
            var usingAddedClass = AddUsing(ornamentedMethodClass, ornamentedMethodClass);
            return usingAddedClass;
        }

        private static string AddUsing(string inputPath, string outputPath = null) {

            var name = SyntaxFactory.QualifiedName(SyntaxFactory.IdentifierName("System"),
                                    SyntaxFactory.IdentifierName("Diagnostics"));

            var rootSyntax = GetSyntaxTreeRoot(inputPath);
            var newUsing = SyntaxFactory.UsingDirective(name).NormalizeWhitespace();
            if (!rootSyntax.Usings.Select(x => x.ToString()).Contains(newUsing.ToString())) {
                rootSyntax = rootSyntax.AddUsings(newUsing);
            }


            var newFullPath = outputPath ?? CreateWeavedFilePath(inputPath);
            SaveToFile(newFullPath, rootSyntax);
            return newFullPath;
        }

        public static string OrnamentClassMethod(
            string classFullPath,
            string methodToOrnament,
            IEnumerable<StatementSyntax> before = null,
            IEnumerable<StatementSyntax> after = null,
            string outputPath = null) {

            //Get original method
            var originalMethod = GetMethod(classFullPath, methodToOrnament, out var originalRoot);
            var originalStatements = originalMethod.Body.Statements.ToList();

            //Create new method           
            var newStatementsList = new List<StatementSyntax>();
            if (before != null) {
                newStatementsList.AddRange(before.ToList());
            }
            newStatementsList.AddRange(originalStatements);
            if (after != null) {
                newStatementsList.AddRange(after.ToList());
            }
            var newMethod = originalMethod.WithBody(originalMethod.Body.WithStatements(
                new SyntaxList<StatementSyntax>(newStatementsList)));

            //Update method
            var weavedRoot = originalRoot.ReplaceNode(originalMethod, newMethod);


            //Update class
            var newFullPath = outputPath ?? CreateWeavedFilePath(classFullPath);
            SaveToFile(newFullPath, weavedRoot);
            return newFullPath;
        }

        private static void SaveToFile(string filePath, CompilationUnitSyntax syntaxRoot) {
            var weavedCodeStr = syntaxRoot.GetText().ToString();
            File.WriteAllText(filePath, weavedCodeStr);
        }

        private static string CreateWeavedFilePath(string classFullPath) {
            var rootFolder = Path.GetDirectoryName(classFullPath);
            var originalName = Path.GetFileNameWithoutExtension(classFullPath);
            var extension = Path.GetExtension(classFullPath);
            var newName = $"{originalName}_1{extension}";
            var newFullPath = Path.Combine(rootFolder, newName);
            return newFullPath;
        }

        private static CompilationUnitSyntax GetSyntaxTreeRoot(string classFullPath) {
            var originalCode = File.ReadAllText(classFullPath);
            var tree = CSharpSyntaxTree.ParseText(originalCode);
            var root = (CompilationUnitSyntax)tree.GetRoot();
            return root;
        }

        private static MethodDeclarationSyntax GetMethod(string classFullPath, string methodToOrnament, out CompilationUnitSyntax root) {
            root = GetSyntaxTreeRoot(classFullPath);
            var nameSpace = (NamespaceDeclarationSyntax)root.Members.First();
            var classDeclarations = nameSpace.Members.Cast<ClassDeclarationSyntax>();
            var methods = classDeclarations.SelectMany(x => x.Members.OfType<MethodDeclarationSyntax>());
            var originalMethod = methods.SingleOrDefault(x => x.Identifier.Value.ToString() == methodToOrnament);
            return originalMethod;
        }

        public static ExpressionStatementSyntax ConsoleWriteLine(string text) {

            var console = SyntaxFactory.IdentifierName("Console");
            var writeline = SyntaxFactory.IdentifierName("WriteLine");
            return ExpressionStatement(console, writeline, text);
        }

        public static ExpressionStatementSyntax ConsoleWrite(string text) {

            var console = SyntaxFactory.IdentifierName("Console");
            var writeline = SyntaxFactory.IdentifierName("Write");
            return ExpressionStatement(console, writeline, text);
        }

        private static ExpressionStatementSyntax ExpressionStatement(
            IdentifierNameSyntax objectIdentifier, 
            IdentifierNameSyntax methodIdentifier, 
            string textArgument) {

            var memberaccess = SyntaxFactory.MemberAccessExpression(
                SyntaxKind.SimpleMemberAccessExpression,
                objectIdentifier,
                methodIdentifier);

            var argument = SyntaxFactory.Argument(
                SyntaxFactory.LiteralExpression(
                    SyntaxKind.StringLiteralExpression,
                    SyntaxFactory.Literal(textArgument)));

            var argumentList = SyntaxFactory.SeparatedList(new[] { argument });

            var writeLineCall =
                SyntaxFactory.ExpressionStatement(
                SyntaxFactory.InvocationExpression(memberaccess,
                SyntaxFactory.ArgumentList(argumentList)));

            return writeLineCall;
        }
    }
}
