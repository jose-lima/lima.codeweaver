﻿using System;
using System.Diagnostics;
using System.IO;

namespace Lima.CodeWeaver.Compiler {

    class Program {

        private static readonly string compilerPath = @"C:\Program Files (x86)\MSBuild\14.0\Bin\csc.exe";


        static void Main(string[] args) {

            //
            var rootPath = @"C:\Users\user1\source\repos\CodeWeaver\Lima.CodeWeaver.TestCode";

            //Weaving core
            var classToCompileName = "Program.cs";
            var classPath = Path.Combine(rootPath, classToCompileName);

            //var beforeStatements = new[] { Weaver.ConsoleWriteLine("Start") };
            //var afterStatements = new[] { Weaver.ConsoleWriteLine("Finished") };
            //var newClassPath = Weaver.OrnamentClassMethod(
            //    classPath,
            //    "Method1",
            //    beforeStatements,
            //    afterStatements);

            var newClassPath = Weaver.TimeClassMethod( classPath, "Method1");
            newClassPath = Weaver.TimeClassMethod( newClassPath, "Method2", newClassPath);

            //Prepare arguments
            var outputExe = Path.Combine(rootPath, "Program_weaved.exe");
            args = new[] { newClassPath, $"-out:{outputExe}" };
            var argsString = string.Join(" ", args);

            //Compile
            Console.WriteLine($"Starting '{compilerPath} {argsString}'...");
            ExecuteProcess(compilerPath, argsString);
            Console.WriteLine($"Program finished.");
            Console.ReadLine();
        }

        private static void ExecuteProcess(string processPath, string argsString) {

            var proc = new Process {
                StartInfo = new ProcessStartInfo {
                    FileName = processPath,
                    Arguments = argsString,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            proc.Start();
            proc.WaitForExit();

            while (!proc.StandardOutput.EndOfStream) {
                string line = proc.StandardOutput.ReadLine();
                Console.WriteLine(line);
            }
        }
    }
}
