﻿using System;
using System.Threading;

namespace Lima.CodeWeaver.TestCode {

    public class Program {

        public static void Main(string[] args) {

            var obj1 = new Class1();
            obj1.Method1();
            obj1.Method2();
            obj1.Method1();
            Console.ReadLine();
        }
    }



    public class Class1
    {
        public void Method1() {
            Thread.Sleep(500);
        }

        public void Method2() {
            Thread.Sleep(300);
        }
    }
}
